# s1tilling_singularity

Singularity definition file for HPC use of s1tiling

## Description

This Singularity container is based on a Docker project from CNES : https://github.com/CNES/S1Tiling

This tool generate time series of calibrated, ortho-rectified and filtered Sentinel-1 images with ortho-rectification application from the Orfeo Tool Box.

## Download and processing

S1tiling container use Eodag to download Sentinel1 images from PEPS or other provider of your choice.

All downloading and processing information must be set up in a configuration file whose template is S1Processor.cfg

S1Processor.cfg must be placed in /data folder of the container.

## Authentication

There are two ways to enter your login information for downloading images.

In a .yml file to put in /data directory of the container
```
peps:
    auth:
        credentials:
            username: peps_id
            password: peps_password
```

By environment variables : to be filled in the container definition file or to be passed as parameters at the container execution
```
EODAG__PEPS__AUTH__CREDENTIALS__USERNAME=peps_id
EODAG__PEPS__AUTH__CREDENTIALS__PASSWORD=peps_password
```
## Build
```
sudo singularity build s1tiling.sif s1tiling.def
```

## Execution
```
singularity run --no-mount tmp -H /data -B /path/to/s1tiling_bind/:/data s1tiling.sif /data/S1Processor.cfg
```
## Author
Cyprien ALEXANDRE

<br/>

![image](docs/espace-dev-ird.png)
